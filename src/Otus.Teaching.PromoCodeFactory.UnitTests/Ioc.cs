﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using PartnerPromoCodeLimit = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.PartnerPromoCodeLimit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class Ioc
    {
        private static readonly IServiceProvider _serviceProvider;

        static Ioc()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddDbContext<DataContext>(options => options.UseInMemoryDatabase("InMemory"), ServiceLifetime.Scoped);
            serviceCollection.AddScoped<IRepository<Partner>, EfRepository<Partner>>();
            serviceCollection.AddScoped<PartnersController>();
            serviceCollection.AddScoped<PartnerFactory>();
            serviceCollection.AddScoped<PartnerPromoCodeLimitFactory>();
            serviceCollection.AddScoped<SetPartnerPromoCodeLimitRequestFactory>();

            _serviceProvider = serviceCollection
                .BuildServiceProvider();
        }

        public static T Resolve<T>() => _serviceProvider.GetRequiredService<T>();
    }
}