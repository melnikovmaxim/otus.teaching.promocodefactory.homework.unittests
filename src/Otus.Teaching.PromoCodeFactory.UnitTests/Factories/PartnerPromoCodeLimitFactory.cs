﻿using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Dsl;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories.Base;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories
{
    public class PartnerPromoCodeLimitFactory: BaseFactory<PartnerPromoCodeLimit>
    {
        public PartnerPromoCodeLimit GetNotCancelledLimitWithoutPartner() => _fixtureComposer
            .Without(l => l.CancelDate)
            .Without(l => l.Partner)
            .Create();
        
        public IEnumerable<PartnerPromoCodeLimit> GetLimitsWithoutPartner() => _fixtureComposer
            .Without(l => l.Partner)
            .CreateMany();
        
        public IEnumerable<PartnerPromoCodeLimit> GetLimitsWithoutPartner(int limits) => _fixtureComposer
            .Without(l => l.Partner)
            .CreateMany(limits);
    }
}