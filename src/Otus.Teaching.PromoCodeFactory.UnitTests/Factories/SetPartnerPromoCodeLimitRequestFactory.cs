﻿using System.Collections.Generic;
using AutoFixture;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories.Base;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories
{
    public class SetPartnerPromoCodeLimitRequestFactory: BaseFactory<SetPartnerPromoCodeLimitRequest>
    {
        public SetPartnerPromoCodeLimitRequest GetRequest() => _fixtureComposer
            .Create();
        
        public SetPartnerPromoCodeLimitRequest GetRequest(int limit) => _fixtureComposer
            .With(r => r.Limit, limit)
            .Create();
    }
}