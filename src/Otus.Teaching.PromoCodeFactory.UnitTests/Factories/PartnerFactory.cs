﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.Dsl;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Factories.Base;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories
{
    public class PartnerFactory: BaseFactory<Partner>
    {
        public Partner GetActivePartnerWithoutLimits() => _fixtureComposer
            .With(p => p.IsActive, true)
            .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
            .Create();
        
        public Partner GetInactivePartnerWithoutLimits() => _fixtureComposer
            .With(p => p.IsActive, false)
            .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
            .Create();
        
        public Partner GetActivePartner(IEnumerable<PartnerPromoCodeLimit> limits) => _fixtureComposer
            .With(p => p.IsActive, true)
            .With(p => p.PartnerLimits, limits.ToList())
            .Create();
        
        public Partner GetActivePartner(IEnumerable<PartnerPromoCodeLimit> limits, int issuedPromoCodes) => _fixtureComposer
            .With(p => p.IsActive, true)
            .With(p => p.PartnerLimits, limits.ToList())
            .With(p => p.NumberIssuedPromoCodes, issuedPromoCodes)
            .Create();
        
        public Partner GetActivePartner(PartnerPromoCodeLimit limit) => _fixtureComposer
            .With(p => p.IsActive, true)
            .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>() { limit })
            .Create();
    }
}