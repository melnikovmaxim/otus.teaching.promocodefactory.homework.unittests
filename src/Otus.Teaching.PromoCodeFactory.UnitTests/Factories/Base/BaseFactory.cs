﻿using AutoFixture;
using AutoFixture.Dsl;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Factories.Base
{
    public abstract class BaseFactory<T>
        where T: class
    {
        protected readonly ICustomizationComposer<T> _fixtureComposer;

        protected BaseFactory()
        {
            _fixtureComposer = new Fixture().Build<T>();
        }
    }
}