﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AutoFixture;
using AutoFixture.Dsl;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.UnitTests.Fixtures;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests: IClassFixture<SetPartnerPromoCodeLimitAsyncTestsFixture>
    {
        private readonly PartnersController _partnersController;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly SetPartnerPromoCodeLimitAsyncTestsFixture _testFixture;

        public SetPartnerPromoCodeLimitAsyncTests(SetPartnerPromoCodeLimitAsyncTestsFixture testFixture)
        {
            _partnerRepository = Ioc.Resolve<IRepository<Partner>>();
            _partnersController = Ioc.Resolve<PartnersController>();
            _testFixture = testFixture;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotExists_StatusCode404()
        {
            // Arrange
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest();
            var partnerGuid = Guid.NewGuid();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerGuid, request);
            var statusCodeResult = result as StatusCodeResult;

            // Assert
            statusCodeResult.Should().NotBeNull();
            statusCodeResult!.StatusCode.Should().Be(StatusCodes.Status404NotFound);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_BlockedPartner_StatusCode400()
        {
            // Arrange
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest();
            var inactivePartner = _testFixture.PartnerFactory.GetInactivePartnerWithoutLimits();

            // Act
            await _partnerRepository.AddAsync(inactivePartner);

            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(inactivePartner.Id, request);
            var statusCodeResult = result as ObjectResult;

            // Assert
            statusCodeResult.Should().NotBeNull();
            statusCodeResult!.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Theory]
        [InlineData(10)]
        public async void SetPartnerPromoCodeLimitAsync_PromoCodesCountGreaterThanZero_ResetPromoCodesCount(int issuedPromoCodes)
        {
            // Arrange
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest();
            var limitNotCancelled = _testFixture.PartnerPromoCodeLimitFactory.GetNotCancelledLimitWithoutPartner();
            var partnerLimits = _testFixture.PartnerPromoCodeLimitFactory
                .GetLimitsWithoutPartner()
                .ToList();

            partnerLimits.Add(limitNotCancelled);

            var partner = _testFixture.PartnerFactory.GetActivePartner(partnerLimits, issuedPromoCodes);

            await _partnerRepository.AddAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var partnerWithoutPromoCodes = await _partnerRepository.GetByIdAsync(partner.Id);

            partnerWithoutPromoCodes.Should().NotBeNull();
            partnerWithoutPromoCodes.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHaveNotCancelledLimit_PreviousLimitCancelled()
        {
            // Arrange
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest();
            var partnerLimit = _testFixture.PartnerPromoCodeLimitFactory.GetNotCancelledLimitWithoutPartner();
            var partner = _testFixture.PartnerFactory.GetActivePartner(partnerLimit);
            await _partnerRepository.AddAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var partnerFromRepository = await _partnerRepository.GetByIdAsync(partner.Id);

            partnerFromRepository.PartnerLimits.First(l => l.Id == partnerLimit.Id).CancelDate
                .Should()
                .NotBeNull();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(int.MaxValue)]
        public async void SetPartnerPromoCodeLimitAsync_CorrectRequest_ShouldBePositiveLimit(int limit)
        {
            // Arrange
            var partner = _testFixture.PartnerFactory.GetActivePartnerWithoutLimits();
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest(limit);
            await _partnerRepository.AddAsync(partner);
            
            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var expectedPartner = await _partnerRepository.GetByIdAsync(partner.Id);

            expectedPartner.Should().NotBeNull();
            expectedPartner.PartnerLimits
                .Should()
                .NotBeNull()
                .And
                .HaveCountGreaterThan(0);
            expectedPartner.PartnerLimits.First(l => l.CancelDate is null).Limit
                .Should()
                .BeGreaterThan(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CorrectRequest_LimitShouldBeSavedInDb()
        {
            // Arrange
            var request = _testFixture.SetPartnerPromoCodeLimitRequestFactory.GetRequest();
            var partner = _testFixture.PartnerFactory.GetActivePartnerWithoutLimits();
            await _partnerRepository.AddAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var expectedPartner = await _partnerRepository.GetByIdAsync(partner.Id);

            expectedPartner.Should().NotBeNull();
            expectedPartner.PartnerLimits.Should().NotBeNull();
            expectedPartner.PartnerLimits.Should().HaveCountGreaterThan(0);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(10)]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHaveZeroOrGreaterLimits_ShouldReturnLimits(int limitCount)
        {
            // Arrange
            var limits = _testFixture.PartnerPromoCodeLimitFactory.GetLimitsWithoutPartner(limitCount);
            var partner = _testFixture.PartnerFactory.GetActivePartner(limits);
            await _partnerRepository.AddAsync(partner);

            // Act
            var result = await _partnersController.GetPartnerLimitsAsync(partner.Id);
            
            // Assert
            var okResult = result.Result as OkObjectResult;
            okResult.Should().NotBeNull();

            var expectedLimits = okResult!.Value as IEnumerable<PartnerPromoCodeLimitResponse>;
            expectedLimits
                .Should()
                .NotBeNull()
                .And
                .HaveCount(limitCount);
        }
    }
}