﻿using Otus.Teaching.PromoCodeFactory.UnitTests.Factories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using PartnerPromoCodeLimit = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement.PartnerPromoCodeLimit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Fixtures
{
    public class SetPartnerPromoCodeLimitAsyncTestsFixture
    {
        public readonly PartnerFactory PartnerFactory;
        public readonly PartnerPromoCodeLimitFactory PartnerPromoCodeLimitFactory;
        public readonly SetPartnerPromoCodeLimitRequestFactory SetPartnerPromoCodeLimitRequestFactory;
        
        public SetPartnerPromoCodeLimitAsyncTestsFixture()
        {
            PartnerFactory = Ioc.Resolve<PartnerFactory>();
            PartnerPromoCodeLimitFactory = Ioc.Resolve<PartnerPromoCodeLimitFactory>();
            SetPartnerPromoCodeLimitRequestFactory = Ioc.Resolve<SetPartnerPromoCodeLimitRequestFactory>();
        }
    }
}